from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from papers import views as papers_views

urlpatterns = [
    path('', include('papers.urls', namespace='papers')),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/register', papers_views.register, name='register'),
    path('accounts/<int:pk>/', papers_views.UserDetailView.as_view(), name='user-detail'),
    path('accounts/<int:pk>/edit', papers_views.user_update, name='user-update'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
        path('rosetta/', include('rosetta.urls')),
    ] + urlpatterns
