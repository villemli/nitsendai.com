from .base import *

DEBUG = True

INSTALLED_APPS += [
    'debug_toolbar',
    'rosetta',
]

INTERNAL_IPS = ('127.0.0.1', 'localhost',)

MIDDLEWARE += [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]
