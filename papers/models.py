from functools import partial

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _, pgettext_lazy

from taggit.managers import TaggableManager

from papers.validators import file_size, file_type


class Author(models.Model):
    user = models.OneToOneField(User, verbose_name=_('user'), on_delete=models.CASCADE, null=True, blank=True)
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=150)
    email_address = models.EmailField(_('email address'), blank=True)
    affiliation = models.CharField(_('affiliation'), max_length=128)
    postal_address = models.CharField(_('postal address'), max_length=128, blank=True)
    phone_number = models.CharField(_('phone number'), max_length=32, blank=True)

    class Meta:
        verbose_name = _('author')
        verbose_name_plural = _('authors')

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    def get_absolute_url(self):
        return reverse('papers:author-detail', args=[str(self.id)])


@receiver(post_save, sender=User)
def update_author(sender, instance, created, **kwargs):
    if created:
        Author.objects.create(user=instance)
    instance.author.save()


class Paper(models.Model):
    title = models.CharField(pgettext_lazy("Paper's title", 'title'), max_length=255)
    abstract = models.TextField(pgettext_lazy("Paper's abstract", 'abstract'))
    keywords = TaggableManager(verbose_name=_('keywords'), blank=True)
    authors = models.ManyToManyField(
        Author,
        verbose_name=_('authors')
    )
    file = models.FileField(_('file'),
                            validators=[partial(file_size, settings.MAX_FILE_SIZE),
                                        partial(file_type, settings.ALLOWED_MIME_TYPES)],
                            help_text=_('Maximum size: %(size)s MiB. Filetypes: %(filetypes)s.' % {
                                'size': settings.MAX_FILE_SIZE,
                                'filetypes': ", ".join(settings.ALLOWED_FILE_EXTENSIONS)}))
    filename = models.CharField(_('filename'), max_length=255)
    created_by = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=pgettext_lazy('Paper was created by', 'created by'),
    )
    created_at = models.DateTimeField(pgettext_lazy("paper's creation time", 'created at'), auto_now_add=True)
    modified_at = models.DateTimeField(pgettext_lazy("paper's modification time", 'modified at'), auto_now=True)

    class Meta:
        verbose_name = _('paper')
        verbose_name_plural = _('papers')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('papers:paper-detail', args=[str(self.id)])
