from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm
from papers.models import Author, Paper
from django.utils.translation import gettext_lazy as _


class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ['first_name',
                  'last_name',
                  'email_address',
                  'affiliation',
                  'postal_address',
                  'phone_number']


class PaperForm(ModelForm):
    class Meta:
        model = Paper
        fields = ['title',
                  'abstract',
                  'keywords',
                  'file']


class RegisterForm(UserCreationForm):
    email = forms.EmailField(label=_('email address'),
                             help_text=_('This can be used to reset your password.'),
                             required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class UserUpdateForm(ModelForm):
    email = forms.EmailField(label=_('email address'),
                             help_text=_('This can be used to reset your password.'),
                             required=True)

    class Meta:
        model = User
        fields = ('email',)

    def save(self, commit=True):
        user = super(UserUpdateForm, self).save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user
