from functools import partial

from django import template


register = template.Library()


def get_field(object, field_name):
    return object._meta.get_field(field_name)


def get_field_value(object, field):
    return getattr(object, field.attname)


@register.filter
def get_field_verbose_name(object, field_name):
    return get_field(object, field_name).verbose_name


@register.inclusion_tag('includes/render_fields.html')
def render_fields(object, field_names):
    def name_value_tuple(field):
        return (field.verbose_name,
                get_field_value(object, field))

    names_and_values = map(name_value_tuple,
                           map(partial(get_field, object),
                               field_names))
    return {'fields': list(names_and_values)}
