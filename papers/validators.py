import magic

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def file_size(max_size, file):
    if file.size > max_size * 1024 ** 2:
        raise ValidationError(
            _('File too large: %(file)s (%(file_size).1f MiB).'),
            code='file_too_large',
            params={'file': file,
                    'file_size': file.size / (1024 ** 2)},
        )


def file_type(allowed_types, file):
    mimetype = magic.from_buffer(file.read(), mime=True)
    if mimetype not in allowed_types:
        raise ValidationError(
            _('Invalid file type: %(file)s.'),
            code='file_type_invalid',
            params={'file': file},
        )
