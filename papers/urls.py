from django.urls import path

from papers import views

app_name = 'papers'
urlpatterns = [
    path('', views.PaperListView.as_view(), name='paper-list'),
    path('paper/<int:pk>/', views.PaperDetailView.as_view(), name='paper-detail'),
    path('paper/<int:pk>/edit', views.paper_update, name='paper-update'),
    path('paper/<int:pk>/download', views.paper_download, name='paper-download'),
    path('paper/<int:pk>/delete', views.PaperDeleteView.as_view(), name='paper-delete'),
    path('paper/new', views.paper_create, name='paper-create'),
    path('author/<int:pk>/', views.AuthorDetailView.as_view(), name='author-detail'),
]
