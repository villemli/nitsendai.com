from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from papers.models import Author, Paper


class AuthorInline(admin.StackedInline):
    model = Author
    can_delete = False
    verbose_name_plural = _('author')


class UserAdmin(BaseUserAdmin):
    inlines = (AuthorInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Paper)
admin.site.register(Author)
