import magic
from urllib.parse import quote_plus

from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms import modelformset_factory
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse_lazy
from django.views.generic import DeleteView, DetailView, ListView

from rules.contrib.views import permission_required, objectgetter, PermissionRequiredMixin

from papers.forms import AuthorForm, PaperForm, RegisterForm, UserUpdateForm
from papers.models import Author, Paper


class SimpleDetailView(DetailView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['fields'] = self.fields
        return context


class PaperDetailView(PermissionRequiredMixin, DetailView):
    model = Paper
    permission_required = 'papers.access_paper'


class PaperDeleteView(PermissionRequiredMixin, DeleteView):
    model = Paper
    permission_required = 'papers.access_paper'
    success_url = reverse_lazy('papers:paper-list')


class PaperListView(ListView):
    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            if user.is_staff:
                return Paper.objects.all()
            else:
                return Paper.objects.filter(
                    Q(created_by=user) |
                    Q(authors__id=user.author.id)
                ).distinct()
        else:
            return Paper.objects.none()


@login_required
def paper_create(request):
    AuthorFormSet = modelformset_factory(
        Author,
        form=AuthorForm,
        extra=1,
        can_delete=True
    )
    first_author = request.user.author
    paper_form_prefix = "paper_form"
    author_formset_prefix = "author_formset"

    if request.method == 'POST':
        paper_form = PaperForm(request.POST, request.FILES, prefix=paper_form_prefix)
        author_formset = AuthorFormSet(request.POST, request.FILES, prefix=author_formset_prefix)

        if paper_form.is_valid() and author_formset.is_valid():
            paper = paper_form.save(commit=False)
            paper.created_by = request.user
            paper.filename = request.FILES[f'{paper_form_prefix}-file'].name
            paper.save()

            authors = author_formset.save()
            authors.insert(0, first_author)
            for author in authors:
                paper.authors.add(author)

            paper_form.save_m2m()

            return redirect('papers:paper-detail', pk=paper.pk)
    else:
        paper_form = PaperForm(prefix=paper_form_prefix)
        author_formset = AuthorFormSet(prefix=author_formset_prefix, queryset=Author.objects.none())
    return render(request, 'papers/paper_form.html', {
        'author_formset': author_formset,
        'paper_form': paper_form,
        'first_author': first_author,
    })


@permission_required('papers.access_paper', fn=objectgetter(Paper, 'pk'))
def paper_update(request, pk):
    AuthorFormSet = modelformset_factory(
        Author,
        form=AuthorForm,
        extra=1,
        can_delete=True
    )
    paper = get_object_or_404(Paper, pk=pk)
    first_author = paper.created_by.author
    paper_form_prefix = "paper_form"
    author_formset_prefix = "author_formset"

    if request.method == 'POST':
        paper_form = PaperForm(request.POST, request.FILES, prefix=paper_form_prefix, instance=paper)
        author_formset = AuthorFormSet(request.POST, request.FILES, prefix=author_formset_prefix)

        if paper_form.is_valid() and author_formset.is_valid():
            old_filename = paper.filename
            paper = paper_form.save(commit=False)
            if 'file' in paper_form.changed_data:
                paper.filename = request.FILES[f'{paper_form_prefix}-file'].name
            else:
                paper.filename = old_filename

            paper.save()

            authors = author_formset.save()
            authors.insert(0, first_author)
            for author in authors:
                paper.authors.add(author)

            paper_form.save_m2m()

            return redirect('papers:paper-detail', pk=paper.pk)
    else:
        paper_form = PaperForm(prefix=paper_form_prefix, instance=paper)
        editable_authors = paper.authors.exclude(pk=first_author.id)
        author_formset = AuthorFormSet(prefix=author_formset_prefix,
                                       queryset=editable_authors)
    return render(request, 'papers/paper_form.html', {
        'author_formset': author_formset,
        'paper_form': paper_form,
        'first_author': first_author,
    })


@permission_required('papers.access_paper', fn=objectgetter(Paper, 'pk'))
def paper_download(request, pk):
    paper = get_object_or_404(Paper.objects.only('file', 'filename'), pk=pk)

    if settings.DEBUG:
        from django.views.static import serve
        filename = paper.file.name
        return serve(request, filename, settings.MEDIA_ROOT)

    original_filename = quote_plus(paper.filename)
    real_filename = quote_plus(paper.file.name)
    mimetype = magic.from_file(paper.file.path, mime=True)

    response = HttpResponse(content_type=mimetype)
    response['Content-Disposition'] = f'attachment; filename*=utf-8\'\'{original_filename}'
    response['X-Accel-Redirect'] = f'/protected/{real_filename}'
    return response


class AuthorDetailView(PermissionRequiredMixin, SimpleDetailView):
    model = Author
    permission_required = 'papers.access_author'
    fields = ['first_name',
              'last_name',
              'email_address',
              'affiliation',
              'postal_address',
              'phone_number',
              ]


class UserDetailView(PermissionRequiredMixin, SimpleDetailView):
    model = User
    permission_required = 'papers.access_user'
    queryset = User.objects.select_related('author')
    fields = {'user': ['email'],
              'author': ['first_name',
                         'last_name',
                         'email_address',
                         'affiliation',
                         'postal_address',
                         'phone_number',
                         ]}


def register(request):
    if request.method == 'POST':
        user_form = RegisterForm(request.POST)
        author_form = AuthorForm(request.POST)

        if user_form.is_valid() and author_form.is_valid():
            user = user_form.save()
            user.refresh_from_db()
            author_form = AuthorForm(request.POST, instance=user.author)
            author_form.full_clean()
            author_form.save()

            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('papers:paper-list')
    else:
        user_form = RegisterForm()
        author_form = AuthorForm()
    return render(request, 'registration/user_form.html', {
        'user_form': user_form,
        'author_form': author_form
    })


@permission_required('papers.access_user', fn=objectgetter(User, 'pk'))
def user_update(request, *args, **kwargs):
    user_id = kwargs.get('pk')
    user = User.objects.select_related('author').get(pk=user_id)
    author = user.author

    if request.method == 'POST':
        user_form = UserUpdateForm(request.POST, instance=user)
        author_form = AuthorForm(request.POST, instance=author)

        if user_form.is_valid() and author_form.is_valid():
            user = user_form.save()
            user.refresh_from_db()
            author_form.save()
            return redirect('user-detail', pk=user_id)
    else:
        user_form = UserUpdateForm(instance=user)
        author_form = AuthorForm(instance=author)
    return render(request, 'registration/user_form.html', {
        'user_form': user_form,
        'author_form': author_form
    })
