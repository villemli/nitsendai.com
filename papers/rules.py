from rules import add_perm, is_staff, predicate


@predicate
def is_coauthor(user, target_author):
    our_papers = user.author.paper_set.all()
    their_papers = target_author.paper_set.all()
    # We use & operator to get the intersection of the paper sets.
    common_papers = our_papers & their_papers
    return common_papers.exists()


@predicate
def is_paper_creator(user, paper):
    return paper.created_by == user


@predicate
def is_same_author(user, target_author):
    return user.author == target_author


@predicate
def is_same_user(user, target_user):
    return user == target_user


add_perm('papers.access_paper', is_paper_creator | is_staff)
add_perm('papers.access_user', is_same_user | is_staff)
add_perm('papers.access_author', is_same_author | is_coauthor | is_staff)
