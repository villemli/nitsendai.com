from mimetypes import guess_all_extensions
from itertools import chain
import re

import taggit


def file_extensions(mimetypes):
    return list(flatmap(guess_all_extensions, mimetypes))


def flatmap(func, items):
    return chain.from_iterable(map(func, items))


def parse_tags(tagstring):
    '''Return tags parsed from a string containing Japanese commas.

    Replace Japanese commas ('、' and '，') in tagstring with ASCII commas (',')
    and return the tags parsed by django-taggit's default tag parser.
    '''
    fixed_tagstring = re.sub('[、，]', ',', tagstring)
    return taggit.utils._parse_tags(fixed_tagstring)
